Library ieee;
Use ieee.std_logic_1164.all;
Use ieee.numeric_std.all;
---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: ads1281_interface_low_level.vhd
-- File history:
--      1.0: 1.07.2014: redesign of ads1281_interface_low_level
--
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@Handles the low_level communication with the ads1281
--	
-- Interfaces to the Texas Instrument ADS1281 ADC	
-- 
-- 
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Oliver Bitterling
--
----------------------------------------------------------------------------------------------------------
entity ads1281_interface_low_level_true is 
port (
	clk				:	In std_logic;
	rst 			: 	In std_logic;
	-- SPI interface
	sclk 			: 	out std_logic;
	adc_sdat		:	in std_logic;
	adc_din			:	out	std_logic;
	--adc control pins
	drdy 			: 	in std_logic;
	pinmode 		: 	out std_logic;
	dr0				:	out std_logic;
	dr1				:	out std_logic;
	phs				:	out std_logic;
	sync 			: 	out std_logic;
	mflag			: 	in std_logic; -- not used
	
	--high level signals
	cmd				:	in std_logic_vector(3 downto 0);
	regread			:	out std_logic_vector(7 downto 0);
	regwrite 		:	in std_logic_vector(7 downto 0); -- not yet used
	command_ll		:	in std_logic_vector(7 downto 0);
	dready_timeouts	:	out integer Range 0 to 100;
	busy			:	out std_logic;
	
	debugport		:	out std_logic_vector(3 downto 0);
	pdata_ready 	:	out std_logic; --pulses high if pdata is ready
	pdata 			: 	out signed(23 downto 0)
	
	
);
end ads1281_interface_low_level_true;

Architecture struct of ads1281_interface_low_level_true is

signal serial_clock					:	std_logic;
signal serial_clock_old				:	std_logic;

Type clock_states is (IDLE, CLOCK_HIGH, CLOCK_LOW);
signal current_clock_state			:	clock_states;
signal next_clock_state				:	clock_states;

Type transmission_states is (IDLE2, WAIT_FALLING_EDGE, WAIT_RISING_EDGE);
signal curren_transmission_state	:	transmission_states;
signal next_transmission_state		:	transmission_states;

Type command_states is (IDLE3, WAIT_STATE, SEND_BYTE_NMBR, RECEIVE_DATA, READ_BYTE_FINISHED, 
						WRITE_BYTE, WRITE_BYTE_FINISHED, WAIT_DREADY, WAIT_FOR_STOP_CLOCK);
signal current_command_state		:	command_states;
signal future_state					:	command_states;

signal wait_cnt						:	integer Range 0 to 660000;
signal long_timeout					:	Integer Range 0 to 660000;


signal start_clock					:	std_logic;
signal stop_clock					:	std_logic;

Constant clock_cycle				:	integer := 14;
signal clock_cnt					:	integer Range 0 to 14;
signal clock_cnt_inc				:	std_logic;
signal clock_cnt_rst				:	std_logic;
	
signal bit_cnt						:	integer Range 0 to 24;
signal bit_cnt_dec					:	std_logic;
signal bit_cnt_set					:	std_logic;

constant register_default				:	std_logic_vector(79 downto 0):= x"40000000000003320862";
	
signal data_to_send					:	std_logic_vector(7 downto 0);
	
signal data_received				:	std_logic_vector(23 downto 0) := (others => '0');
signal receive_bit					:	std_logic;

signal cmd_old						:	std_logic_vector(3 downto 0);
signal fast_busy					:	std_logic;
signal busy_reg						:	std_logic;

signal nmbr_bits					:	integer Range 8 to 24;
signal transmit_data				:	std_logic;
signal nmbr_bytes					:	integer Range -1 to 10;
signal cmd_read_write				:	std_logic;
signal read_write					:	std_logic;
signal sync_drdy					:	std_logic;
signal drdy_sync					:	std_logic;
signal sync_adc_sdat				:	std_logic;
signal adc_sdat_sync				:	std_logic;
signal timeout_cnt					:	integer Range 0 to 10010;
signal timeout_watchdod_cnt			:	integer Range 0 to 100;

signal adc_din_comb					:	std_logic;




Begin

dr0 	<= '0';
dr1 	<= '0';
phs 	<= '0';
pinmode <= '0';
sync	<= '1';


busy 			<= fast_busy or busy_reg;
fast_busy 		<= '1' when cmd /= cmd_old else '0';
dready_timeouts	<= timeout_watchdod_cnt;

Process(clk,rst)
Begin
	if rising_edge(clk) then
		if rst = '1' then
			current_clock_state			<= IDLE;
			curren_transmission_state	<= IDLE2;
			serial_clock_old			<= '0';
			cmd_old						<= (others => '0');
			clock_cnt					<= 0;
			bit_cnt						<= 0;
			data_received				<= (others => '0');
			sync_drdy					<= '0';
			sync_adc_sdat               <= '0';
			drdy_sync	                <= '0';
			adc_sdat_sync               <= '0';
		else
			current_clock_state			<= next_clock_state;
			curren_transmission_state	<= next_transmission_state;
			
			serial_clock_old			<= serial_clock;
			sclk 						<= serial_clock;
			cmd_old						<= cmd;
			
			if clock_cnt_rst = '1' then
				clock_cnt	<= 0;
			elsif clock_cnt_inc = '1' then
				clock_cnt	<= clock_cnt +1;
			end if;
						
			if bit_cnt_set = '1' then
				bit_cnt	<= nmbr_bits;
			elsif bit_cnt_dec = '1' then
				bit_cnt	<= bit_cnt -1;
			end if;
			
			if receive_bit = '1' then 
				data_received(bit_cnt-1)	<= adc_sdat_sync; --- to much sync ?
			end if;

			
			-- synchonisation of external signal
			sync_drdy			<= drdy;
			sync_adc_sdat		<= adc_sdat;
			drdy_sync			<= sync_drdy;
			adc_sdat_sync		<= sync_adc_sdat;
			adc_din				<= adc_din_comb;

		
		end if;
	end if;
end process;

Command : process(all)
Begin

	if rising_edge(clk) then
		if rst = '1' then
			timeout_watchdod_cnt	<= 0;
			busy_reg				<= '0';
			transmit_data			<= '0';
			nmbr_bits				<= 8;
			current_command_state	<= IDLE3;
			future_state			<= IDLE3;
			wait_cnt				<= 0;
			data_to_send			<= (others => '0');
			read_write				<= '0';
			cmd_read_write			<= '0';
			nmbr_bytes				<= 9;
			debugport				<= "0000";
			long_timeout			<= 660000;
			pdata					<= (others => '0');
		else
			--defaults
			transmit_data			<= '0';
			
			Case current_command_state is
			
				when IDLE3 =>
					pdata_ready 					<= '0';
					debugport						<= "0001";
					case cmd is
						   
						when "0001" => 		--send command
							busy_reg				<= '1';
							read_write				<= '1';
							transmit_data			<= '1';
							nmbr_bits				<= 8;
							current_command_state	<= WAIT_STATE;
							future_state			<= IDLE3;
							wait_cnt				<= 480;
							data_to_send			<= command_ll;
						when "1000" =>		--read register
							busy_reg 				<= '1';
							current_command_state	<= WAIT_STATE;
							read_write				<= '1';
							transmit_data			<= '1';
							nmbr_bits				<= 8;
							wait_cnt				<= 480;
							data_to_send			<= command_ll;
							nmbr_bytes				<= 0; --read one byte from register
							future_state			<= SEND_BYTE_NMBR;
							cmd_read_write			<= '0';	--read data 
							
						when "1011" => 		-- read all registers
							busy_reg 				<= '1';
							current_command_state	<= WAIT_STATE;
							read_write				<= '1';
							transmit_data			<= '1';
							nmbr_bits				<= 8;
							wait_cnt				<= 480;
							data_to_send			<= x"21"; -- start to read from address x"01"
							nmbr_bytes				<= 9; --read 10 registers
							future_state			<= SEND_BYTE_NMBR;
							cmd_read_write			<= '0';	--read data 
							
						when "0110"	=>	-- write all regsiters
							busy_reg 				<= '1';
							current_command_state	<= WAIT_STATE;
							read_write				<= '1';
							transmit_data			<= '1';
							nmbr_bits				<= 8;
							wait_cnt				<= 480;
							data_to_send			<= x"41"; -- start to write from address x"01"
							nmbr_bytes				<= 9; --read 10 registers
							future_state			<= SEND_BYTE_NMBR;
							cmd_read_write			<= '1';	--write data 
						when "0111" =>	-- Read on command
							busy_reg				<= '1';
							current_command_state	<= WAIT_STATE;
							future_state			<= WAIT_DREADY;
							wait_cnt				<= 208;
							data_to_send			<= x"12";
							read_write				<= '1';
							transmit_data			<= '1';
							nmbr_bits				<= 8;
							timeout_cnt				<= 10010;

						   
						when "1010" =>
							current_command_state	<= IDLE3;
						when others =>
							current_command_state	<= IDLE3;
					end case;
			
			when WAIT_DREADY => -- wait for conversion data to arrive
				debugport					<= "0010";
				current_command_state		<= WAIT_DREADY;
				pdata_ready 				<= '0';
				if drdy_sync = '0' then
					read_write				<= '0';
					transmit_data			<= '1';
					nmbr_bits				<= 24;
					current_command_state	<= WAIT_FOR_STOP_CLOCK; 
				else
					if timeout_cnt > 0 then
						timeout_cnt				<= timeout_cnt -1;
					else
						timeout_watchdod_cnt	<= timeout_watchdod_cnt +1;
						timeout_cnt				<= 10010;
						transmit_data			<= '1';
					end if;	
				end if;
				if cmd = "0010" then
						current_command_state	<= IDLE3;
						timeout_watchdod_cnt	<= 0;
						busy_reg				<= '0';
				end if;
				
			when WAIT_FOR_STOP_CLOCK => -- wait until conversion data is fully clocked out and then read it into register
				debugport					<= "0011";
				current_command_state		<= WAIT_FOR_STOP_CLOCK;
				timeout_watchdod_cnt		<= 0;
				if stop_clock = '1' then
					pdata					<= signed(data_received);
					pdata_ready				<= '1';
					busy_reg				<= '0';
					current_command_state	<= IDLE3;
				end if;
					

			when SEND_BYTE_NMBR => -- send the nummber of bytes to be writen or read
				debugport				<= "0100";
				current_command_state	<= WAIT_STATE;
				read_write				<= '1';
				transmit_data			<= '1';
				nmbr_bits				<= 8;
				wait_cnt				<= 480;
				data_to_send			<= std_logic_vector(to_unsigned(nmbr_bytes,8));
				if cmd_read_write = '1' then	
					future_state		<= WRITE_BYTE;
				else
					future_state		<= RECEIVE_DATA;
				end if;
				
			when WRITE_BYTE =>
				debugport				<= "0101";
				data_to_send			<= register_default(79-nmbr_bytes*8 downto 72-(nmbr_bytes)*8 );
				read_write				<= '1';
				transmit_data			<= '1';
				nmbr_bits				<= 8;
				wait_cnt				<= 480;
				current_command_state	<= WAIT_STATE;
				future_state			<= WRITE_BYTE_FINISHED;
				
			when WRITE_BYTE_FINISHED =>
				debugport					<= "0110";							
				nmbr_bytes					<= nmbr_bytes -1;
				if nmbr_bytes = 0 then
					current_command_state	<= WAIT_STATE;
					future_state			<= IDLE3;
					wait_cnt				<= long_timeout;
				else
					current_command_state	<= WRITE_BYTE;
				end if;
			
			when RECEIVE_DATA => -- produces clocks to read one byte
				debugport				<= "0111";
				pdata_ready 			<= '0';
				read_write				<= '0';
				transmit_data			<= '1';
				nmbr_bits				<= 8;
				wait_cnt				<= 240;
				current_command_state	<= WAIT_STATE;
				future_state			<= READ_BYTE_FINISHED;
				
			when READ_BYTE_FINISHED =>
				debugport				<= "1000";
				pdata_ready 			<= '1';
				regread					<= data_received(7 downto 0);
				nmbr_bytes				<= nmbr_bytes -1;
				wait_cnt				<= 240;
				current_command_state	<= WAIT_STATE;
				if nmbr_bytes = 0 then
					future_state		<= IDLE3;
				else
					future_state		<= RECEIVE_DATA;
				end if;
				
			when WAIT_STATE =>
				debugport				<= "1001";
				current_command_state	<= WAIT_STATE;
				if wait_cnt > 0 then
					wait_cnt	<= wait_cnt -1;
				else
					current_command_state	<= future_state;
					if future_state = IDLE3 then
						busy_reg				<= '0';		
					end if;
				end if;
			
			when others =>
				debugport				<= "1010";
				current_command_state	<= IDLE3;
				
			end case;
		end if;
	end if;
end process;

Process(all) -- create serial_clock
Begin
	--defaults
	serial_clock			<= '0';
	clock_cnt_inc			<= '0';
	clock_cnt_rst			<= '0';
	
	case current_clock_state is
	
		when IDLE =>
			next_clock_state		<= IDLE;
			if start_clock = '1' then
				next_clock_state	<= CLOCK_LOW;
			end if;
			
		when CLOCK_HIGH =>
			next_clock_state		<= CLOCK_HIGH;
			serial_clock			<= '1';
			if clock_cnt < clock_cycle -1 then
				clock_cnt_inc		<= '1';
			else
				clock_cnt_rst		<= '1';
				next_clock_state	<= CLOCK_LOW;		
			end if;
			
		when CLOCK_LOW =>
			next_clock_state		<= CLOCK_LOW;
			if clock_cnt < clock_cycle -1 then
				clock_cnt_inc		<= '1';
			else
				clock_cnt_rst		<= '1';
				next_clock_state	<= CLOCK_HIGH;
			end if;
			if stop_clock = '1' then
					next_clock_state<= IDLE;
					clock_cnt_rst	<= '1';
			end if;
	end case;
end process;

process(all) -- send one byte
Begin
	--defaults
	start_clock			<= '0';
	stop_clock			<= '0';
	adc_din_comb		<= '0';
	bit_cnt_set			<= '0';
	bit_cnt_dec			<= '0';
	receive_bit			<= '0';
		
	case curren_transmission_state is
	
		when IDLE2 =>
			next_transmission_state		<= IDLE2;
			if transmit_data = '1' then
				start_clock			<= '1';
				bit_cnt_set			<= '1';
				next_transmission_state	<= WAIT_RISING_EDGE;
			end if;
				
		when WAIT_RISING_EDGE =>
			next_transmission_state		<= WAIT_RISING_EDGE;
			if read_write = '1' then
					adc_din_comb			<= data_to_send(bit_cnt-1);
			else
					receive_bit				<= '1';
			end if;
			if serial_clock = '1' and serial_clock_old = '0' then
				next_transmission_state	<= WAIT_FALLING_EDGE;
			end if;
			
		when WAIT_FALLING_EDGE =>
			next_transmission_state		<= WAIT_FALLING_EDGE;
			if read_write = '1' then
				adc_din_comb			<= data_to_send(bit_cnt-1);
			end if;
			if serial_clock = '0' and serial_clock_old = '1' then
				next_transmission_state	<= WAIT_RISING_EDGE;
				bit_cnt_dec			<= '1';
				if bit_cnt = 1 then
					next_transmission_state<= IDLE2;
					stop_clock			<= '1';
				end if;
			end if;	
		
		when others =>
			next_transmission_state		<= IDLE2;
	end case;
End Process;

End;

