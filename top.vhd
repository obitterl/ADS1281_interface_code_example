LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
library synplify;
use synplify.attributes.all;
library proasic3e;
use proasic3e.components.all;

library nDGlib;
use nDGlib.nDGconstants_pkg.all;
use nDGlib.rad_control_pkg.all;
use nDGlib.nDG_records.all;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: top.vhd
-- File history:
--      0.1:	11/07/2011:	First version for nDG, based on v92 nIPQ top...
--				 	
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
-- 				Provisorische top level entity um zu zeigen wie beide ADC interface module zusammen hängen.
--	
-- 
--				
--
-- 
--				
--
--
-- 
-- 
--
----------------------------------------------------------------------------------------------------------

entity top is

port (

	------------board resources-------------------------------------
	ptop_clk_in 		:		in std_logic;	--input clock	
	ptop_reset_sw		:		in std_logic;
	dready_gater		:		in std_logic; --debug input		
	
	ptop_DQAMG_cmd0		:		in std_logic;
	ptop_DQAMG_cmd1		:		in std_logic;
	
	ptop_ledport		:		out std_logic_vector(7 downto 0);	--led port (blinks)
	
	--dready_gater		:		In 	std_logic;			
	ptop_debugport 		:		out std_logic_vector(17 downto 0);
    
    ptop_heartbeat		:		out std_logic;
	ptop_watchdog		: 		in std_logic;
	
	ptop_dready_gate0	: 		in std_logic;
	ptop_dready_gate1	: 		in std_logic;
	
	----------------Small Flash Interface------------------------------------
	ptop_flash_sm_so		:in std_logic;
	ptop_flash_sm_hold		:out std_logic;
	ptop_flash_sm_clk		:out std_logic;
	ptop_flash_sm_si		:out std_logic;
	ptop_flash_sm_cs		:out std_logic;
	ptop_flash_sm_wp		:out std_logic;
	
	----------------Big Flash Interface------------------------------------
	ptop_flash_big0_so		:in std_logic;
	ptop_flash_big0_hold	:out std_logic;
	ptop_flash_big0_clk		:out std_logic;
	ptop_flash_big0_si		:out std_logic;
	ptop_flash_big0_cs		:out std_logic;
	ptop_flash_big0_wp		:out std_logic;
	
	ptop_flash_big1_so		:in std_logic;
	ptop_flash_big1_hold	:out std_logic;
	ptop_flash_big1_clk		:out std_logic;
	ptop_flash_big1_si		:out std_logic;
	ptop_flash_big1_cs		:out std_logic;
	ptop_flash_big1_wp		:out std_logic;
	
	ptop_flash_big2_so		:in std_logic;
	ptop_flash_big2_hold		:out std_logic;
	ptop_flash_big2_clk		:out std_logic;
	ptop_flash_big2_si		:out std_logic;
	ptop_flash_big2_cs		:out std_logic;
	ptop_flash_big2_wp		:out std_logic;
	
	----------------ADC interface signals-----------------------------------
	ptop_adc0_sclk				:out std_logic;
	ptop_adc0_din				:out std_logic;
	ptop_adc0_reset				:out std_logic;
	ptop_adc0_pwdn				:out std_logic;
	ptop_adc0_pinmode			:out std_logic;
	ptop_adc0_dr0				:out std_logic;
	ptop_adc0_dr1				:out std_logic;
	ptop_adc0_phs				:out std_logic;
	ptop_adc0_sync				:out std_logic;
	
	ptop_adc0_drdy				:in std_logic;
	ptop_adc0_sdat				:in std_logic;
	ptop_adc0_mflag				:in std_logic;
	
	ptop_adc1_sclk				:out std_logic;
	ptop_adc1_din				:out std_logic;
	ptop_adc1_reset				:out std_logic;
	ptop_adc1_pwdn				:out std_logic;
	ptop_adc1_pinmode			:out std_logic;
	ptop_adc1_dr0				:out std_logic;
	ptop_adc1_dr1				:out std_logic;
	ptop_adc1_phs				:out std_logic;
	ptop_adc1_sync				:out std_logic;
	
	ptop_adc1_drdy				:in std_logic;
	ptop_adc1_sdat				:in std_logic;
	ptop_adc1_mflag				:in std_logic;
	-------------------relays----------------------------
	ptop_RefRel_A		:out std_logic;
	ptop_RefRel_B		:out std_logic;
	
	-----------------logic ouptus------------------------------------
	ptop_I_power			:out std_logic;
	ptop_Quench			:out std_logic;
	
	--------------i2c ports----------------------
	ptop_scl 			: in std_logic;
	ptop_sda				: inout std_logic;
	ptop_i2c_power		:out std_logic;
	
	-----------------SPI ports-----------------------------------------
	--CS0					:in std_logic;
	--SCLK				:in std_logic;
	--SDO					:out std_logic
	----------------Uart ports-------------------------------------------
	--uart_rx				:	In std_logic;
	uart_tx				:	Out std_logic
	
	
	---------------end ports-------------------------------------------------
	);
	
end top;

-------------------------------------------
--Architecture
-------------------------------------------

architecture a of top is


attribute syn_radhardlevel : string;
attribute syn_radhardlevel of a: architecture is rad_control;


component CLKINT
port ( A : in std_logic;
	Y : out std_logic);
end component;



---------------board port signals (top ports as signals-----------------------
------------board resources-------------------------------------
signal	clk_in 			: std_logic;	--input clock	
signal	reset_sw		: std_logic;
	


--adc0 outputs
signal adc0_sclk		: std_logic;
signal adc0_reset		: std_logic;
signal adc0_pwdn		: std_logic;
signal adc0_pinmode	: std_logic;
signal adc0_dr0		: std_logic;
signal adc0_dr1		: std_logic;
signal adc0_phs		: std_logic;
signal adc0_sync		: std_logic;
signal adc0_din		: std_logic;
--adc0inputs
signal adc0_sdat		: std_logic;
signal adc0_mflag		: std_logic;
signal adc0_drdy		: std_logic;
--adc0highlevel	
signal adc0_cmd			: std_logic_vector(3 downto 0);
signal adc0_regadr			: std_logic_vector(3 downto 0);
signal adc0_regread		: std_logic_vector(7 downto 0);
signal adc0_regwrite		: std_logic_vector(7 downto 0);
signal adc0_regwrite_hi	: std_logic_vector(7 downto 0);
signal adc0_busy			: std_logic;
signal adc0_pdata_ready	: std_logic;
signal adc0_pdata			: signed(23 downto 0);
signal adc0_state_debug	: std_logic_vector(2 downto 0);
signal adc0_bcount_debug	: std_logic_vector(3 downto 0);
signal adc0_cmd_hl			: std_logic_vector(3 downto 0);
signal adc0_command_ll		: std_logic_vector(7 downto 0);

signal adcs_command		: std_logic_vector(7 downto 0);
signal adcs_regadr_hi		: std_logic_vector(7 downto 0);
signal adc0failure		:std_logic;
signal adc0dready			: std_logic;




begin




adc0_ll	: entity nDGlib.ads1281_interface_low_level_true
port map (	clk 			=> clk,
			rst				=> reset,
			sclk 			=> adc0_sclk,
			adc_sdat 		=> adc0_sdat,
			adc_din			=> adc0_din,
			drdy			=> adc0_drdy,
			pinmode			=> adc0_pinmode,
			dr0				=> adc0_dr0,
			dr1				=> adc0_dr1,
			phs				=> adc0_phs,
			sync			=> adc0_sync,
			mflag			=> adc0_mflag,
				
			cmd				=> adc0_cmd,
			regread			=> adc0_regread,
			regwrite		=> adc0_regwrite,
			command_ll		=> adc0_command_ll,
			dready_timeouts	=> dready_timeouts1,
			busy			=> adc0_busy,
			debugport		=> debugport0(3 downto 0),
			pdata_ready 	=> adc0_pdata_ready,
			pdata			=> adc0_pdata
);


adc0_hl	: entity nDGlib.ads1281_interface_high_level_new
port map( 	clk 			=> clk,
			rst 			=> reset,
			cmd_hi			=> adcs_command(7 downto 4),
			cmd 			=> adc0_cmd,
			busy_hi			=> busy_hi0,
			adc_failure		=> adc0failure,
			regread			=> adc0_regread,
			reg_read_hi		=> adc0_regread_hi,
			reg_adr_hi		=> adcs_regadr_hi(7 downto 4),
			command_ll		=> adc0_command_ll,
			reg_write_hi	=> adc0_regwrite_hi,
			regwrite 		=> adc0_regwrite,
			dready_timeouts	=> dready_timeouts1,
			Powerdown		=> adc0_pwdn,
			reset			=> adc0_reset,
			busy			=> adc0_busy,
			pdata_ready		=> adc0_pdata_ready,
			pdata_low_level => adc0_pdata,
			adc_data 		=> Udiff_raw,
			state_debug		=> debugport0(7 downto 4),
			dreadypulse		=> adc0dready
);



end;