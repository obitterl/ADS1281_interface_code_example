library ieee ;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
library nDGlib;
use nDGlib.nDGconstants_pkg.all;
---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: ads1281_interface_low_level.vhd
-- File history:
--      0.1: 18.04.2013: first attempt
--
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief handles the high level communication with the ADS1281 
--	
-- Interfaces to the Texas Instrument ADS1281 ADC
-- Enthält mehrer Schutzmaßnahmen die überprüfen ob der ADC von Strahlung beeinfluss wurde. 
-- Das umfasst die Überwachung der Konfigurationsdaten des ADCs und ein Neustart im Fall des Ausbleibens von Daten. 	
-- 
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Oliver Bitterling
-- Co. Author : Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity ads1281_interface_high_level_new is 
port (
	clk, rst : in std_logic;
	
	cmd_hi				:	in 	std_logic_vector(3 downto 0);
	reg_adr_hi 			:	in 	std_logic_vector(3 downto 0);
	reg_write_hi 		:	in 	std_logic_vector(7 downto 0);
	reg_read_hi			:	Out std_logic_vector(7 downto 0);
	--interupt			:	in	std_logic;
	busy_hi				:	Out	std_logic;
	adc_failure			:	Out std_logic;
	--high level signals connect to ads1281_interface_low_level
	cmd					: 	out std_logic_vector(3 downto 0);
	command_ll			:	out std_logic_vector(7 downto 0);
	regread				: 	in 	std_logic_vector(7 downto 0);
	regwrite 			: 	out std_logic_vector(7 downto 0);
	dready_timeouts		:	in	integer Range 0 to 100;
	busy				: 	in 	std_logic;
	powerdown			:	out	std_logic;
	reset				:	out std_logic;
	pdata_ready 		: 	in 	std_logic;
	pdata_low_level 	: 	in 	signed(23 downto 0);
	adc_data 			: 	out signed(23 downto 0);
	state_debug 		:	out std_logic_vector(3 downto 0);
	dreadypulse 		: 	out std_logic
);
end ads1281_interface_high_level_new;


architecture ads1281_interface_high_level_arch of ads1281_interface_high_level_new is

type state_type is (reset_wait, reset1, reset2, reset3, idle, waitforbusy, send_restart, command_finished,
					wait_for_conversion_data, wait_for_register_data, verify_reg_cont, fix_register_cont, 
					powerdown_adc, wait_for_adc_restart, register_write_finished, adc_failure_state);
signal state : state_type;

signal pdata_ready_old		:	std_logic;
signal register_cnt			:	integer Range 0 to 9;
signal register_cont		:	std_logic_vector(79 downto 0);
signal register_default		:	std_logic_vector(79 downto 0):= x"40000000000003320862";
signal wait_cnt				:	integer Range 0 to 40000000;
signal powerdown_cnt		:	integer Range 0 to 400000;
signal device_restart_cnt	:	integer Range 0 to 640000;
signal adc_failure_cnt		:	integer Range 0 to 10;
signal pwdn_rst_switch		:	std_logic;


begin

regwrite 	<= reg_write_hi;
reg_read_hi	<= regread;


fsm: process(rst, clk)
begin
	
	if rising_edge(clk) then
		if rst = '1' then
			state 				<= reset_wait;
			adc_data 			<= (others => '0');
			dreadypulse 		<= '0';
			pdata_ready_old		<= '0';
			--wait_cnt			<= 4600;
			wait_cnt			<= ads1281_startup_delay; --1s 
			cmd					<= "1010";
			powerdown			<= '1';
			powerdown_cnt		<= ads1281_powerdown_time;
			device_restart_cnt	<= 4000;
			adc_failure_cnt		<= 0;
			state_debug			<= "0000";
			reset				<= '0';
			adc_failure			<= '0';
			pwdn_rst_switch		<= '0';
			
		else
			pdata_ready_old	<= pdata_ready;
			busy_hi			<= '1';
			reset			<= '1';
			
			case state is
				when reset_wait =>
					state_debug		<= "0001";
					if wait_cnt > 0 then
						wait_cnt	<= wait_cnt-1;
						state		<= reset_wait;
					else
						state		<= reset1;
					end if; 
				
				when reset1 =>
					state			<= reset1;
					state_debug		<= "0010";
					if busy = '0' then
						cmd			<="0110"; 	--restore all registers
						state		<= reset2;
					end if;
					
				when reset2 =>
					state_debug		<= "0011";
					state			<= reset2;
					cmd				<= "0001";--stop continuous readg
					command_ll		<= x"11";
					if busy = '0' then
						state		<= reset3;
					end if;
				
					
				when reset3 =>
					state_debug		<= "0100";
					state			<= reset3;
					cmd				<= "0111"; -- Read data on command
					if busy = '0' then
						state		<= wait_for_conversion_data;
					end if;

					
				when idle =>
					state_debug		<= "0101";
					busy_hi			<= '0';
					case cmd_hi is
						when x"0" =>	--Wakeup
							cmd			<= "0001";
							command_ll	<= x"00";
							state		<= command_finished;
						when x"1" =>	--Standby
							cmd			<= "0001";
							command_ll	<= x"02";
							state		<= command_finished;
						when x"2" =>	--Sync
							cmd			<= "0001";
							command_ll	<= x"04";
							state		<= command_finished;
						when x"3" =>	--Reset
							cmd			<= "0001";
							command_ll	<= x"06";
							state		<= command_finished;
						when x"4" =>	-- Stop Continuous Read
							cmd			<= "0001";
							command_ll	<= x"11";
							state		<= command_finished;
						when x"5" => 	--read data by command
							cmd			<= "0111";
							state		<= wait_for_conversion_data;-------------------------------------------------------------
						when x"6" =>	-- Read Register
							cmd			<= "1000"; 
							command_ll	<= "0010" & reg_adr_hi;
							state		<= command_finished;
						when x"7" =>	-- Offset Callibration
							cmd			<= "0001";
							command_ll	<= x"60";
							state		<= command_finished;
						when x"8" =>	-- Gain Callibration
							cmd			<= "0001";
							command_ll	<= x"61";
							state		<= command_finished;                         												
						when x"9" => -- read all registers
							cmd 		<= "1011";
							state		<= command_finished;
						when x"A" => --do nothing
							state 		<= idle;
							cmd			<= "1010";
						when x"B" => --switch reset method to powerdown pin
							state 			<= idle;
							pwdn_rst_switch	<= '0';
						when x"c" => --switch reset method to reset pin
							state			<= idle;
							pwdn_rst_switch	<= '1';
						when others =>
							state <= idle;
					end case;
					
				when wait_for_conversion_data =>
					state_debug			<= "0110";
					cmd					<= "1011";  --read all registers
					state 				<= wait_for_conversion_data;
					if busy = '0' then ------------------------------------------------------------------
						dreadypulse		<= '1';		
						adc_data 		<= pdata_low_level;
						state 			<= wait_for_register_data;
						register_cnt	<= 0;
					end if;
					
					if dready_timeouts = 2 then
						if adc_failure_cnt = 0 then
							state			<= powerdown_adc;
							cmd				<= "0010"; --set low level interface to idle
							adc_failure_cnt	<= adc_failure_cnt +1;
						else
							state			<= adc_failure_state;
							cmd				<= "0010"; --set low level interface to idle
						end if;
							
					end if;
					
					if cmd_hi = x"A" then
						state 			<= idle;
					end if;
					
				when powerdown_adc => 
					state_debug			<= "0111";
					if pwdn_rst_switch = '0' then
						powerdown			<= '0';
					else
						reset				<= '0';
					end if;
					state				<= powerdown_adc;
					
					if powerdown_cnt > 0 then
						powerdown_cnt	<= powerdown_cnt -1;
					else
						state 			<= wait_for_adc_restart;
						powerdown_cnt	<= ads1281_powerdown_time;
						if pwdn_rst_switch = '0' then
							powerdown			<= '1';
						else
							reset				<= '1';
						end if;
					end if;
				
				
					
				when wait_for_adc_restart =>
					state_debug				<= "1000";
					state					<= wait_for_adc_restart;
					if device_restart_cnt > 0 then
						device_restart_cnt	<= device_restart_cnt -1;
					else
						device_restart_cnt	<= 4000;
						state 				<= register_write_finished;
						cmd					<= "0110"; -- rewrite configuration into register
					end if;
					
				when register_write_finished => 
					state_debug				<= "1001";
					state					<= register_write_finished;
					cmd						<= "0001";--stop continuous read
					command_ll				<= x"11";
					if busy = '0' then 
						state				<= reset3;
					end if;
					

				when wait_for_register_data =>
					state_debug			<= "1010";
					cmd					<= "1010";
					state				<= wait_for_register_data;
					adc_failure_cnt		<= 0;
					if pdata_ready = '1' and pdata_ready_old = '0' then
						if register_cnt = 9 then
							register_cont((register_cnt+1)*8-1 downto register_cnt*8) <= regread;
							state		<= verify_reg_cont;
						else
							register_cont((register_cnt+1)*8-1 downto register_cnt*8) <= regread;
							register_cnt<= register_cnt +1;
						end if;
					end if;
					
				when verify_reg_cont =>
					state_debug		<= "1011";
					state			<= verify_reg_cont;
					if busy = '0' then ------------------------------------------------------------------
						if register_default = register_cont then
							state		<= wait_for_conversion_data;
							cmd			<= "0111";
							dreadypulse	<= '0';
						else
							state		<= fix_register_cont;
							cmd			<= "0110";
						end if;
					end if;
					
				
				when fix_register_cont =>
					state_debug		<= "1100";
					state			<= fix_register_cont;
					cmd				<= "0001";--stop continuous read
					command_ll		<= x"11";
					if busy = '0' then 
						dreadypulse	<= '0';
						state		<= reset3;
					end if;
						
				
				when command_finished =>
					state_debug	<= "1101";
					state		<= command_finished;
					cmd			<= "1010";
					if busy = '0' then
						state 	<= idle;
					end if;
		
				when adc_failure_state =>
					state		<= adc_failure_state;
					adc_failure	<= '1';
				when others =>
					state_debug	<= "1110";
					state 		<= idle;
					
			end case;
		end if;
	end if;
end process;


end; 
